# README #

pre-proposal: https://docs.google.com/document/d/1cFSzxZnOGZ_vcAZIjI5WRWsI45g1H0SkCMx6Idutl-M/edit?usp=sharing

proposal: https://docs.google.com/document/d/1pI14Ngr9-en4j0JbscbwY-qiVkZ0F-ACKaTTMhTMLIw/edit?usp=sharing

design document: https://docs.google.com/document/d/1kAbfM9oEtxUIhZ2LLCEblhHcSXthcAch7sIZKmLBQlA/edit?usp=sharing

report: https://docs.google.com/document/d/1LR5M09Jb8IyqR6zGTFEx6Sk97gLNn0ObeZrxQEyLlbg/edit?usp=sharing

Transpiler in SharpTS

Sample code in Clipper, HelloWorld, CrossFileReferences, SimpleOOP (transpiled files are .ts files next to the .cs files)

Clipper: run npm install, then npm run start:dev, then navigate to localhost:80 to view demo

Poster slides: https://docs.google.com/presentation/d/12rSymggU1Ky6IuhUqo1PB2A1BSYwkwqH_pAR1Tlt3R0/edit?usp=sharing

Video: https://drive.google.com/file/d/1v-OfTP3kOJGGFM06mmeLfwzkpSmq7HS-/view?usp=sharing
