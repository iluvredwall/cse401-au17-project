﻿using System;

namespace SimpleOOP {
   public class Program {
      static void Main(string[] args) {
         LinkedListDemo.Test();
         ArrayDemo.Test();
         OverloadingDemo.Test();
         PolymorphismDemo.Test();
         OutRefDemo.Test();
      }
   }
}
